#Configure
##Prerequisites
Please be shure to have the following Tools on your Device
1. vagrant with following plugins
	i. vagrant-vbguest
	ii. vagrant-hostmanager
	iii. vagrant-hostsupdater
	iv. vagrant-share

##Setup
Now you can run: `curl -L http://rove.io/install | bash` to set up your vagrant box with the required dependencies

After everything is setup and done go inside the box and do the following
```
vagrant ssh
cd /vagrant
npm install
npm install supervisor -g
supervisor server.js
```
now everything should be up and running
and you can run datagenerator.jar to fill the mongodb with some data

#See some Magic happen
Direct your Browser to `localhost:3000/map` and you should see the map.